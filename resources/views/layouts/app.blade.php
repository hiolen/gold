<!DOCTYPE html>
<html lang="en">
<head>
   <title>Gold</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/font-awesome.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/style.css')}}">
    <link href='https://fonts.googleapis.com/css?family=PT+Serif:400,700italic,700,400italic' rel='stylesheet' type='text/css'>
</head>
<body>
    <header class="container-fluid">
        <div class="header">
            <nav class="navbar">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed btn btn-primary" data-toggle="collapse" data-target="#main-menu" aria-expanded="false">
                        <span class="menu">MENU</span>
                    </button>
                  <a class="navbar-brand" href="#"><img src="{{asset('assets/images/logo.png')}}" alt=""></a>
                </div>
                <div class="collapse navbar-collapse" id="main-menu">
                    <ul class="nav navbar-nav main-menu">
                        <li><a href="{{url('/')}}">Home</a></li>
                        <li><a href="{{url('/book')}}">Book</a></li>
                        <li><a href="{{url('/author')}}">Author</a></li>
                        <li><a href="{{url('/contact')}}">Contact</a></li>
                        <li><a href="{{url('/blog')}}">Blog</a></li>
                    </ul>
                    <ul class="nav navbar-nav social">
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    </ul>
                </div><!-- /.navbar-collapse -->
                <a href="javascript:void(0);" class="btn-side"><i class="fa fa-align-right"></i></a>
            </nav>
            <div class="side-bar text-center">
                <a href="javascript:void(0);" class="btn-side"><i class="fa fa-times"></i></a>
                <div class="side-book">
                    <h3 class="side-title">Daddy's Girls</h3>
                    <p class="book-author">A book by Suzanne Gold</p>
                    <img src="{{asset('assets/images/side-book.png')}}" alt="" class="img-responsive">
                    <p class="description hidden-xs">Daddy’s Girls is a bittersweet tale of love, spirit and redemption in a dysfunctional family. A mother and her two daughters tell their own stories in overlapping vignettes that create a vivid patchwork of life's defining moments to reveal the dark forces lurking beneath their middle-class veneer. </p>
                    <p class="description visible-xs">Daddy’s Girls is a bittersweet tale of love, spirit and redemption in a dysfunctional family. A mother and her two daughters tell their own</p>
                    <button class="btn btn-primary">BUY THE BOOK</button>
                </div>
                <div class="side-author">
                    <img src="{{asset('assets/images/author-side.png')}}" alt="" class="img-responsive">
                    <h3 class="side-title">Suzzane Gold</h3>
                    <p class="author-info">
                        <a href="mailto:suzzanegold@gmail.com" target="_blank">suzzanegold@gmail.com</a> <br> <a href="tel:+6393292391023" target="_blank">093292391023</a></p>
                    <button class="btn btn-primary">contact</button>
                </div>
                <div class="side-blog">
                    <h3 class="side-title">Latest BLog</h3>
                    <div class="post">
                        <p class="posted">January 20,2015 <br> Uncategorized</p>
                        <p class="blog-name">Surviving A <br> Dysfunctional Family.</p>
                        <img src="{{asset('assets/images/side-blog.jpg')}}" alt=" " class="img-responisve">
                    </div>
                    <div class="post">
                        <p class="posted">January 20,2015 <br> Uncategorized</p>
                        <p class="blog-name">Surviving A <br> Dysfunctional Family.</p>
                        <img src="{{asset('assets/images/side-blog.jpg')}}" alt=" " class="img-responisve">
                    </div>
                    <button class="btn btn-primary">SEE MORE</button>
                </div>
            </div>
        </div>
    </header>

    @yield('content')

    <footer class="footer">
        <div class="container text-center">
            <div class="row">
                <div class="col-xs-12 col-md-6">
                    <div class="row">
                        <div class="col-xs-6 col-md-6">
                            <span class="title">SUZANNE GOLD</span>
                            <p class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
                        </div>
                        <div class="col-xs-6 col-md-6">
                            <span class="title">CONTACT</span>
                            <p class="description">
                                <a href="mailto:Suzzannegold@gmail.com" target="_blank">suzzannegold@gmail.com</a><br><a href="tel:+6393292391023" target="_blank">093292391023</a>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6">
                    <div class="row">
                        <div class="col-xs-6 col-md-6">
                            <a href="#quick-links" class="collapsed" data-toggle="collapse" data-target="#quick-links" aria-expanded="false">
                                <span class="title quick-links">QUICK LINKS
                                    <i class="fa fa-plus"></i>
                                    <i class="fa fa-minus"></i>
                                </span></a>
                            <ul class="list-unstyled collapse" id="quick-links">
                                <li><a href="homepage.html">Home</a></li>
                                <li><a href="book.html">Book</a></li>
                                <li><a href="author.html">Author</a></li>
                                <li><a href="blog.html">Blog</a></li>
                                <li><a href="contact.html">Contact</a></li>
                            </ul>
                        </div>
                        <div class="fcol-newslet col-xs-6 col-md-6">
                            <span class="title">NEWSLETTER</span>
                            <form class="news-form">
                            <input type="text" id="e-mail" placeholder="Email Address">
                            <input value="SUBMIT" class="btn btn-submit" type="submit" />
                            </form>
                            <ul class="nav social">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            </ul>
                      </div>
                    </div>
                </div>
            </div>
            <span class="copy-right">ALL RIGHTS RESERVED© 2016</span>
        </div>
    </footer>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/js/script.js')}}"></script>
</body>
</html>
