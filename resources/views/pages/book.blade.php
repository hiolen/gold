@extends('layouts.app')

@section('content')
<div class="content text-center">
	<div class="container">
		<div class="book-head">
			<div class="row">
				<div class="col-xs-12 text-center">
					<div class="book-top">
						<div class="star">
							<img src="{{asset('assets/images/star.png')}}" alt="">
						</div>
						<p class="book-award">
							ForeWord Magazine's Book of the
							<span class="enter"> Year Award Gold Medal Winner for Fiction</span></p>
						<div class="star">
							<img src="{{asset('assets/images/star.png')}}" alt="">
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6">
					<div class="book-left">
						<img src="{{asset('assets/images/book.png')}}" alt="" class="img-responsive">
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 text-left">
					<div class="book-right">
						<h1 class="book-title visible-md">Daddy's Girls</h1>
						<h1 class="book-title hidden-md">Daddy's Girls</h1>
						<p class="book-author"><i>A book by Suzanne Gold</i></p>
						<span class="book-price">Paperback $12.00 <br>Hardcover $13.00</span>
						<button class="btn btn-primary">BUY THE BOOK</button>
					</div>
				</div>
			</div>
		</div>
		<div class="book-content">
			<div class="title">
            	<span class="text-center">ABOUT THE AUTHOR</span>
            </div>
            <p class="description h4">Daddy's Girls is a rich yet simple tale of love, madness and spirit, told by the three women in a family on the verge of mental illness. Overlapping vignettes reveal the dark forces beneath their middle-class veneer as they struggle to love one another. From her dual perspective as a psychologist and sister of a schizophrenic, Suzanne Gold portrays insanity as a metaphor for spiritual purpose.</p>
            <div class="title">
            	<span class="text-center">EXCERPT</span>
            </div>
            <p class="description h4">THE LAST TIME I saw Cherie she was still beautiful.</p>
            <p class="description h4">I used to envy her perfect nose, her perfect teeth. Even her artificially- straight artificially-blonde hair flattered her. I dread seeing her now. Mom says Cherie's mental state deteriorated fast after her boyfriend/cocaine connection dumped her, and soon she was seeing black helicopters following her, international conspiracies spying on her. When she ran out of money and tried to get the boyfriend to take her back -- yelling on his doorstep, threatening, and frightening the neighbors -- he called the police. They threw her into Egg Harbor State Hospital, where she's been for over a year, court-committed, with reviews every six months.</p>
            <p class="description h4">Lost in memory, I stare out the airplane window, barely registering the dull roar of the engines or the attendants rolling carts down the aisle.  Thirty thousand feet down, barren waves of earth undulate. Probably mountains, but from up here they just look like random abstract patterns. The plane hits an air pocket and I'm falling but my stomach stays behind, leaving me that much more nauseated than I already was about making this trip. I remember Mom calling to tell me about Cherie's hospitalization. Guilt, shame and outrage mingled in her voice.  "The New Jersey State Police, no less. A crowd came to watch." I could picture the way she slits her eyes in disapproval. "Can't say I'm surprised. Cherie always was difficult. But a nervous breakdown in public on a quiet street in Cape May. It's so humiliating!" Mom sounded as if she'd taken Cherie's psychosis as a personal affront, and maybe it was. It was a   long time  coming, although Cherie was twenty-six when the first bona fide symptoms appeared. Late onset adult schizophrenia, the diagnostic manual calls it. Now Mom's torn between tying to fix Cherie and just wanting to forget she ever had a second child.</p>
            <p class="description h4">She'd asked me to come right away, as if I could do anything about it. Too busy, I said, but really I couldn't face it.  </p>
		</div>
	</div>
</div>
@endsection