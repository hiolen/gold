@extends('layouts.app')

@section('content')
<div class="content text-center">
	<div class="contact-content">
		<div class="container">
			<div class="title">
            	<span class="text-center">contact</span>
            </div>
            <div class="author-forms">
            	<div class="row">
	            	<div class="col-xs-12 col-sm-6">
	            		<div class="author">
	            			<img src="{{asset('assets/images/author.jpg')}}" alt="" class="img-responsive">
        					<h2 class="author-name">Suzzane Gold</h2>
        					<span class="author-info">
        						<a href="mailto:suzzanegold@gmail.com" target="_blank">suzzanegold@gmail.com </a><br> 
        						<a href="tel:+6393292391023">093292391023</a>
        					</span>
        					<p class="description h4">Hi, there! Thanks for dropping by.<br>Why don’t you send me a message so we can catch up?</p>
      					</div>
	            	</div>
	            	<div class="col-xs-12 col-sm-6">
	            		<div class="forms text-left">
	            			<div class="form-group">
							  	<label for="usr" class="input-name">Name</label>
							  	<input type="text" class="form-control" id="usr">
							</div>
							<div class="form-group">
							  	<label for="email" class="input-name">Email</label>
							  	<input type="text" class="form-control" id="email">
							</div>
							<div class="form-group">
							  	<label for="message" class="input-name">Message</label>
							  	<textarea class="form-control" rows="5" id="message"></textarea>
							</div>
							<button class="btn btn-primary btn-send">send</button>
	            		</div>
	            	</div>
	            </div>
            </div>
		</div>
	</div>
</div>
@endsection