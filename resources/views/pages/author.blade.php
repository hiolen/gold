@extends('layouts.app')

@section('content')
<div class="content text-center">
      <div class="content-banner">
        <div class="container">
          <div class="author">
            <img src="{{asset('assets/images/author.jpg')}}" alt="" class="img-responsive">
            <h2 class="author-name">Suzzane Gold</h2>
            <p class="author-info">
              <a href="mailto:suzzanegold@gmail.com" target="_blank">suzzanegold@gmail.com</a>
              <span class="separator"> | </span>
              <a href="tel:+6393292391023" target="_blank">093292391023</a>
            </p>
          </div>
        </div>
      </div>
      <div class="author-content">
        <div class="container">
          <div class="about-author">
            <div class="title">
              <span class="text-center">ABOUT THE AUTHOR</span>
            </div>
            <p class="description h4">Suzanne Gold has a Master's degree in Psychology, a certificate in ministry from the Universal Life Church, and many years of study in spiritual principles and methods. She writes a weekly column for United Press International's Religion and Spirituality website, and a blog for the San Francisco Chronicle. She is available for personal and spiritual counseling on life, relationship,  and work issues in person or by telephone.</p>
            <p class="description h4">Suzanne Gold has a Master's degree in Psychology, a certificate in ministry from the Universal Life Church, and many years of study in spiritual principles and methods. She writes a weekly column for United Press International's Religion and Spirituality website, and a blog for the San Francisco Chronicle. She is available for personal and spiritual counseling on life, relationship,  and work issues in person or by telephone.</p>
            <button class="btn btn-primary">contact</button>
          </div>
          <div class="counceling-author">
            <div class="title">
              <span class="text-center">COUNCELING</span>
            </div>
            <p class="description">Suzanne Gold is available for consultation and coaching on life issues, in small group workshops, by telephone or email, especially for families of the mentally ill.</p>
            <button class="btn btn-primary">GET A FREE CONSULTATION</button>
          </div>
        </div>
      </div>
    </div>
@endsection