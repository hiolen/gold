@extends('layouts.app')

@section('content')
<div class="content">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-9">
				<div class="blog-content">
					<div class="row">
						<div class="col-xs-12 col-sm-6">
							<div class="blog-post">
								<span class="blog-date">January 20,2015  | Uncategorized</span>
								<h3 class="blog-title">My First Blog Post</h3>
								<img src="{{asset('assets/images/blog-1.jpg')}}" alt=" " class="img-responsive">
								<p class="description h4">Suzanne Gold has a Master's degree in Psychology, a certificate in ministry from the Universal Life Church, and many years of study in spiritual principles and methods. She writes a weekly column</p>
								<button class="btn btn-primary">CONTINUE READING</button>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6">
							<div class="blog-post">
								<span class="blog-date">January 20,2015  | Uncategorized</span>
								<h3 class="blog-title">My First Blog Post</h3>
								<img src="{{asset('assets/images/blog-1.jpg')}}" alt=" " class="img-responsive">
								<p class="description h4">Suzanne Gold has a Master's degree in Psychology, a certificate in ministry from the Universal Life Church, and many years of study in spiritual principles and methods. She writes a weekly column</p>
								<button class="btn btn-primary">CONTINUE READING</button>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6">
							<div class="blog-post">
								<span class="blog-date">January 20,2015  | Uncategorized</span>
								<h3 class="blog-title">My First Blog Post</h3>
								<img src="{{asset('assets/images/blog-2.jpg')}}" alt=" " class="img-responsive">
								<p class="description h4">Suzanne Gold has a Master's degree in Psychology, a certificate in ministry from the Universal Life Church, and many years of study in spiritual principles and methods. She writes a weekly column</p>
								<button class="btn btn-primary">CONTINUE READING</button>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6">
							<div class="blog-post">
								<span class="blog-date">January 20,2015  | Uncategorized</span>
								<h3 class="blog-title">My First Blog Post</h3>
								<img src="{{asset('assets/images/blog-2.jpg')}}" alt=" " class="img-responsive">
								<p class="description h4">Suzanne Gold has a Master's degree in Psychology, a certificate in ministry from the Universal Life Church, and many years of study in spiritual principles and methods. She writes a weekly column</p>
								<button class="btn btn-primary">CONTINUE READING</button>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6">
							<div class="blog-post">
								<span class="blog-date">January 20,2015  | Uncategorized</span>
								<h3 class="blog-title">My First Blog Post</h3>
								<img src="{{asset('assets/images/blog-3.jpg')}}" alt=" " class="img-responsive">
								<p class="description h4">Suzanne Gold has a Master's degree in Psychology, a certificate in ministry from the Universal Life Church, and many years of study in spiritual principles and methods. She writes a weekly column</p>
								<button class="btn btn-primary">CONTINUE READING</button>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6">
							<div class="blog-post">
								<span class="blog-date">January 20,2015  | Uncategorized</span>
								<h3 class="blog-title">My First Blog Post</h3>
								<img src="{{asset('assets/images/blog-3.jpg')}}" alt=" " class="img-responsive">
								<p class="description h4">Suzanne Gold has a Master's degree in Psychology, a certificate in ministry from the Universal Life Church, and many years of study in spiritual principles and methods. She writes a weekly column</p>
								<button class="btn btn-primary">CONTINUE READING</button>
							</div>
						</div>
						<div class="col-xs-12">
							<div class="old-post">
								<a  href="#" class="btn">&laquo; Older Post</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-md-3">
				<div class="blog-menu text-center">
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-md-12">
							<div class="title">
								<span class="text-center">CATEGORIES</span>
							</div>
							<ul class="list-unstyled">
								<li><a href="#">Life</a></li>
								<li><a href="#">SciFi</a></li>
								<li><a href="#">General</a></li>
								<li><a href="#">Horror</a></li>
								<li><a href="#">Amber</a></li>
								<li><a href="#">Animals</a></li>
								<li><a href="#">Nature</a></li>
							</ul>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-12">
							<div class="title">
								<span class="text-center">QUICK LINKS</span>
							</div>
							<ul class="list-unstyled">
								<li><a href="{{url('/')}}">Home</a></li>
								<li><a href="{{url('/book')}}">Book</a></li>
								<li><a href="{{url('/author')}}">Author</a></li>
								<li><a href="#">Order</a></li>
								<li><a href="{{url('/contact')}}">Contact</a></li>
								<li><a href="{{url('/blog')}}">Blog</a></li>
							</ul>
						</div>
					</div>
					<div class="tags">
						<div class="title">
							<span class="text-center">Tags</span>
						</div>
						<ul class="list-unstyled">
							<li><a href="#">SciFi</a></li>
							<li><a href="#">General</a></li>
							<li><a href="#">Horror</a></li>
							<li><a href="#">Amber</a></li>
							<li><a href="#">SciFi</a></li>
							<li><a href="#">General</a></li>
							<li><a href="#">Horror</a></li>
							<li><a href="#">Amber</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection