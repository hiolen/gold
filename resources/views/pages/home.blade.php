@extends('layouts.app')

@section('content')
<div class="content">
    <div class="container">
        <div class="book-head">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <div class="book-top">
                        <div class="star">
                            <img src="{{asset('assets/images/star.png')}}" alt="">
                        </div>
                        <p class="book-award">
                            ForeWord Magazine's Book of the
                            <span class="enter"> Year Award Gold Medal Winner for Fiction</span></p>
                        <div class="star">
                            <img src="{{asset('assets/images/star.png')}}" alt="">
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6">
                    <div class="book-left">
                        <img src="{{asset('assets/images/book.png')}}" alt="" class="img-responsive">
                    </div>
                </div>
                <div class="col-xs-12 col-md-6 text-left">
                    <div class="book-right">
                        <h1 class="book-title visible-md">Daddy's Girls</h1>
                        <h1 class="book-title hidden-md">Daddy's Girls</h1>
                        <p class="sub-head">A book by Suzanne Gold</p>
                        <p class="description h4">Daddy's Girl is a bittersweet tale of love, spirit and redemption in a dysfunctional family. A mother and her two daughters tell their own stories in overlapping vignettes that create a vivid patchwork of life's defining moments to reveal the dark forces lurking beneath their middle-class veneer.</p>
                        <button class="btn btn-primary">BUY THE BOOK</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="book-reviews">
        <div class="text-center">
            <div class="title">
                <span class="text-center">BOOK REVIEWS</span>
            </div>
            <h3 class="review-title">"The best book I've read since The Hours by Michael Cunningham" <br><span class="h4">- Pulitzer Prize winner 2000</span></h3>
        </div>
        <div class="wrap">  
            <div class="row text-left">
                <div class="reviews-col col-xs-12 col-md-6">
                    <div class="reviews">
                        <p class="description italic">"Writing from personal experience can be cathartic. Writing from the heart with a depth of understanding, placed with dramatic context with the prowess of a good storyteller, can also be a good fiction. Daddy's Girls is a tragic story of loss, frustration, blame and anguish; it's also a heartening tale of love and its resurrection against all odds. Gold suggests possibilities beyond the accepted norm, and lends a new dimension to an old story."</p>
                        <p class="author-italic">- Anne DeGrace<br>Nelson (BC) Daily News</p>
                    </div>
                </div>
                <div class="reviews-col col-xs-12 col-md-6">
                    <div class="reviews">
                        <p class="description italic">"This is not light reading but it is a book that will speak to you on many levels, a book that can alter your perception of the world -- and to me, that's what a good fiction should do -- it should broaden your horizons and urge you to think outside the box. I am serious when I say it is the best one I've read since Cunningham's The Hours. This book is worth your time!"</p>
                        <p class="author-italic">- Terry Mathews<br>Midwest Book Review, BookBrowser.com </p>
                    </div>
                </div>
                <div class="reviews-col col-xs-12 col-md-6">
                    <div class="reviews">
                        <p class="description italic">"Fascinating... with layering of meaning... frightening, yet very funny... written so powerfully that it forces the reader to see (the characters) with compassion... skillfully written and cleverly constructed. You really must read this book to find a new way of looking at a patriarchal society, at families, at women, at death and life and hope and especially madness."</p>
                        <p class="author-italic">- Fran Gillespie<br>MentalHelp.Net</p>
                    </div>
                </div>
                <div class="reviews-col col-xs-12 col-md-6">
                    <div class="reviews">
                        <p class="description italic">"Suzanne Gold has a wonderful ability to write a fascinating story, one I soon won't forget. I was captivated. Each word and thought is carefully written, making it, in my opinion, a must read!"</p>
                        <p class="author-italic">BookReviewCafe.com</p>
                    </div>
                </div>
            </div>
            <div class="text-center">
                <a class="btn btn-primary" href="#" role="button">READ MORE REVIEWS</a>
            </div>
        </div>
    </div>
</div>
<div class="content-mdl">
    <div class="author">
        <div class="content-mdl-top">
            <div class="text-center">
                <img src="{{asset('assets/images/author.jpg')}}" alt="" class="abs">
                <p class="pstyle">Suzzanne Gold</p>
                <p class="pstyle2">
                    <a href="mailto:Suzzannegold@gmail.com" target="_blank">suzzannegold@gmail.com</a> 
                    <span class="separator">| </span>
                    <a href="tel:+6393292391023" target="_blank">093292391023</a>
                </p>
                <p class="pstyle3">Suzzanne Gold has a Master's degree in Psychology, a certificate in Ministry from the Universal Life Church, and many years of study in spiritual principles and methods. She writes a weekly column for United Press International's Religion and Spirituality website, and a blog for the San Francisco Chronicle. She is available for personal and spiritual counseling on life, relationship, and work issues in person or by telephone.</p>
                <a class="btn btn-primary" href="#" role="button">CONTACT</a>
            </div>
        </div>  
        <div class="counseling">
            <div class="title">
                <span class="text-center">Counseling</span>
            </div>
            <div class="counseling-author">
                <p class="text-center">Suzzanne Gold is available for consultation and coaching on life issues, in small group workshops, by telephone or email, especially for families of the mentally ill. The questions I'm asked present patterns. Here are some common stories:</p>
            </div>
            <div class="counseling-col">
                <div class="row">
                    <div class=" counsel-cont col-xs-12 col-md-6">
                        <h4 class="coun-title">My Family Is Driving Me Crazy</h4>
                        <div class="coun-description">
                            <p class="description">Dear Suzzanne,<br>
                            <span class="tab">I had a wonderful therapist in (city) for manyyears, but havinanction, I'm disappointed to be drowning in it again. I really need help.<br>Drowning<br>****</span></p>
                            <div class="text-center"><a class="btn btn-default" href="#" role="button">READ MORE</a></div>
                        </div>
                    </div>
                    <div class="counsel-cont col-xs-12 col-md-6">
                        <h4 class="coun-title">I Made A Bad Decision</h4>
                        <div class="coun-description">
                            <p class="description">Dear Suzzanne,<br>
                            <span class="tab">I made a bad decision recently in deciding totake in my daughter and her two sons after she lost her job and her marriage broke up. All three of them are selfish and sloppy, and they become quite...</span></p>
                            <div class="text-center"><a class="btn btn-default" href="#" role="button">READ MORE</a></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="text-center">
                <a class="btn btn-primary" href="#" role="button">GET A FREE CONSULTATION</a>
            </div>
        </div>
        <div class="interviews">
            <div class="container">
                <div class="title">
                    <span class="text-center">INTERVIEWS</span>
                </div>
                <div class="row text-left">
                    <div class="col-xs-12 col-md-6">
                        <h6><i class="fa fa-microphone"></i> Family Fixer</h6>
                        <p class="description">by Jill Kramer | in the Pacific Sun, September, 2002</p>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <h6><i class="fa fa-microphone"></i> Interview</h6>
                        <p class="description">by Terry Mathews | at BookBrowser.com March 2001</p>
                    </div>
                </div>
                <div class="see-more text-center">
                    <a class="btn" href="#" role="button">See More</a>
                </div>
            </div>
        </div>
        <div class="stor-essay">
            <div class="container">
                <div class="title">
                    <span class="text-center">STORIES AND ESSAYS</span>
                </div>
                <div class="row text-left">
                    <div class="col-xs-12 col-md-6">
                        <h6 class="description"><i class="fa fa-book"></i> "What To Do In Hell"</h6>
                        <p class="description">Finding Revelation in Difficult Circumstances</p>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <h6 class="description"><i class="fa fa-book"></i> "Encountering Sainte Suzanne"</h6>
                        <p class="description">An unexpected contact with divine presence....</p>
                    </div>
                </div>
                <div class="see-more text-center">
                    <a class="btn" href="#" role="button">See More</a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="content text-center">
    <div class="title">
        <span class="text-center">LATEST BLOGS</span>
    </div>
    <div class="container">
        <div class="home-blog">
            <div class="row">
                <div class="col-xs-6 col-sm-6 col-md-3">
                    <div class="blog-post">
                        <p class="blog-date">January 20, 2015 | Uncategorized</p>
                        <h5 class="blog-title">Surviving a Dysfunctional Famil...</h5>
                        <img src="{{asset('assets/images/blogpic.png')}}" alt="" class="img-responsive">
                        <p class="description h4">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do</p>
                        <a class="btn btn-primary" href="#" role="button">READ MORE</a>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-3">
                    <div class="blog-post">
                        <p class="blog-date">January 20, 2015 | Uncategorized</p>
                        <h5 class="blog-title">Surviving a Dysfunctional Famil...</h5>
                        <img src="{{asset('assets/images/blogpic.png')}}" alt="" class="img-responsive">
                        <p class="description h4">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do</p>
                        <a class="btn btn-primary" href="#" role="button">READ MORE</a>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-3">
                    <div class="blog-post">
                        <p class="blog-date">January 20, 2015 | Uncategorized</p>
                        <h5 class="blog-title">Surviving a Dysfunctional Famil...</h5>
                        <img src="{{asset('assets/images/blogpic.png')}}" alt="" class="img-responsive">
                        <p class="description h4">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do</p>
                        <a class="btn btn-primary" href="#" role="button">READ MORE</a>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-3">
                    <div class="blog-post">
                        <p class="blog-date">January 20, 2015 | Uncategorized</p>
                        <h5 class="blog-title">Surviving a Dysfunctional Famil...</h5>
                        <img src="{{asset('assets/images/blogpic.png')}}" alt="" class="img-responsive">
                        <p class="description h4">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do</p>
                        <a class="btn btn-primary" href="#" role="button">READ MORE</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
